# Trust Level Algorithm
## Interface
bool trust(Graph webOfTrust, Array&lt;Person&gt; seeds, Person person);

## Motivation
The algorithm is the heart of the project, really -- the algorithm determines if a person at a given instant can be trusted.

## Features & Considerations
### Boolean value
Although the algorithm might do a lot of fancy math, the ultimate output is just a boolean value. Extra care must be taken if a fixed threshold is defined. A Bayesian approach might be more desirable.

### Friend levels
Similar to PGP, a person can assign different levels of trust to a person:

> * 1 = Don't know
> * 2 = I do NOT trust
> * 3 = I trust marginally
> * 4 = I trust fully

(Source: GnuPG)

For UX reasons, these options will not be presented when the user first "friend" a person -- these choices will be available in "advanced configuration" and the likes.

### Seed values
*Seeds* are "more trusted" persons. This is similar to the seeds in the PageRank algorithm which includes *cnn.com* and *usa.gov*. In the context of possible P2P applications, the crowd of underwriters can be seeds.

## Candidate 1:  PageRank clone
Wikipedia definition:
> PageRank (PR) is an algorithm used by Google Search to rank websites in their search engine results.

### Algorithm ([Source](https://en.wikipedia.org/wiki/PageRank#Power_Method))
![](https://s26.postimg.org/wvz3rubnd/Page_Rank_Wikipedia.png)

### Strengths
- Mathematical beauty
- Takes into account of all nodes in the graph
- Allows "seed values" to bootstrap the trust
- Faster: Complexity in ![](https://wikimedia.org/api/rest_v1/media/math/render/svg/065337b95c6a093f00995ffbc0bef2f8f56078f0)

### Weakness
- Iterative nature makes computation time unpredictable
- Updating part of the graph => whole graph re-calculated

## Candidate 2: Average path length from other nodes in a weighted network

Wikipedia definition:
> Average path length is a concept in network topology that is defined as the average number of steps along the shortest paths for all possible pairs of network nodes. It is a measure of the efficiency of information or mass transport on a network.

### Algorithm
A naive attempt:
```
total := 0
for each node in graph, minus the target node
	calculate the shortest path from "node" to the target node
	accumulate it in total
return total / number of nodes
```

### Strengths
- Not iterative
- "Somewhat" transitive
- Also allows seed values

### Weaknesses
- It's slower - complexity: ![](https://wikimedia.org/api/rest_v1/media/math/render/svg/4fcb7644781d08e9e958d4a430a3107da04bf1b3) for each Dijkstra run, multiply it by **V**, and you get ![](https://wikimedia.org/api/rest_v1/media/math/render/svg/bf71ab4f4ed0f5bff48b7f4ef6de4ef2df24c7dd)
- Updating part of the graph => whole graph re-calculated

## Candidate 3: GnuPG default trust algorithm
### Algorithm
A user is considered trusted if both (1) and (2) is true:

> 1. it is signed by enough valid keys, meaning
> * 1a. you have signed it personally,
> * 1b. it has been signed by one fully trusted key, or
> * 1c. it has been signed by three marginally trusted keys; and
> 2. the path of signed keys leading from K back to your own key is five steps or shorter.

### Strengths
- Heavy stress on local proximity
- Faster computation time, since it only traverse through the whole graph only as needed
- Updating a part of the graph does not require the whole thing to be recomputed
- Separates the concern of "trust in a person" and "trust in the key belonging to who it claims to be".

### Weaknesses
- Does not take into account the whole graph
- Fixed constants are set

## Proposal
The PageRank-like algorithm will be adopted. This gives a more "holistic" view of the whole graph and prevents a small subset of the graph from manipulating the trust level without significant work.

### Algorithm
The power iteration method will be used. Please refer to the algorithm copied above.

"Seeds" will be given additional shares in the initial probability distribution `P`.

You can see my journey tinkering with PageRank using Python [here](https://gitlab.com/isaackwan-fyp/docs/blob/master/pagerank-trial.ipynb).

### Weaknesses
#### Do not support multiple levels of trust
In the beginning of this document (and project) it is planned that multiple levels of acquaintanceship will be supported. The original design of PageRank does not support this.

There are two options to this. Either (1) patch matrix `M` so that PR are distributed unevenly among outgoing links, or (2) drop this support.

In this project approach 2 will be adopted. Firstly, the code can be changed in the future to support this; more importantly, since the friendship registry is now public, it'd be somewhat awkward to maintain this attribute.

#### The intuitive explanation does not apply in WOT
In the original PageRank paper, the authors gave a very elegant explanation of their model: there is an imaginary random surfer who clicks on links randomly -- the PR metric simply reflects the probability of the surfer hitting the page. The higher PR it has, the more trusted and valuable it is.

In our project however, pages are replaced as humans and links are simply acquaintances. A person with more acquaintances might not mean that s/he is more credible -- the opposite might very well be true!

However, the PageRank algorithm does have a role in preventing a small group of user from easily manipulating the trustworthiness metric by taking the whole graph into account.

#### Slow computation
Because solidity, the programming language used for Ethereum smart contracts does not have primitives/library for mathematics (built-in or otherwise), and that SIMD instructions are not available (although there is currently a [proposal](https://github.com/ethereum/EIPs/issues/616) to change this), one must resort to manually doing all the fancy maths the old way. This might mean higher [gas prices](https://www.cryptocompare.com/coins/guides/what-is-the-gas-in-ethereum/). 

## Future Optimization
It is hoped that the implementation can be "swappable" so that future improvements can be made. For example, in the second semester, more sophisticated algorithm can be explored.
