# Topic: Decentralized assertion platform on the blockchain

## Summary
Given an assertion made by a user on the blockchain, how do we determine its trustworthiness in a decentralized and auditable manner? This project aims to answer this taking inspirations from Web of Trust (WOT) from Pretty Good Privacy (PGP). The deliverable in this semester is a library where downstream applications can allows users to assert, attest, and verify a claim.

## Motivation
Blockchains and "smart contracts" allows decentralized systems because they make sure that contractual clauses can be enforced in a secure and auditable manner. For instance, *The DAO* is a Ethereum-based venture capital fund that allows its investors to collectively vote on the majority of its investment decisions, a significant improvement over the traditional way of running a VC.

However, the need to transfer knowledge in the real world into the blockchain often arises in advanced use cases of smart contracts. For instance, a peer-to-peer insurance system might need to verify its insured's claim of loss of limb. This might be easily solved in the real world by having a note signed by a registered doctor; doing this on the blockchain is not as easy. How do one make sure that the insured really did lose its limb? When a "doctor" chimes in, how do one know that s/he indeed is a registered, proper physician?

## Solution
We put forward an identity-based trust platform. Trust or reputation is built upon identity instead of an individual claim. To establish trust, simply ask other identities (which can be either a person, smart contract, etc.) to "friend" you -- similar to how Pretty Good Privacy (PGP)'s Web of Trust (WOT) would work, except that such trust levels are publicized on a public registry. Similar to PGP, again, the trust level is adjustable. Then a claim made by a particular identity is automatically tied to its trust level at that time. Notice that a threshold level is applied as a variable (e.g. decimal from 0 to 1) is converted to a binary value (whether the claim is legit).

## Related work 1
Pretty Good Privacy (PGP) relies on a Web of Trust (WOT) as a decentralized way of maintaining trust among users, instead of a centralised Public Key Infrastructure (PKI) which relies on Certificate Authorities (CAs) for verification. PGP utilizes WOT to maintain a mapping between humans and its associated key. In this project, however, we maintain a mapping between a user (regardless of its real-world identity) and his assertion's trust levels.

## Related work 2
[uPort](https://www.uport.me/) is an identity platform that runs on Ethereum. It aims to "liberate" users from existing authentication schemes, which might involve traditional username/password pairs, OAuth2 implemented as "Sign in with Facebook" and the like.

It allows the user to be "asserted" by other address (which includes other uPort identity or a smart contract) as having a particular attribute. For example, identity A may claim that identity B is born 24 years ago. Identity B can provide A's claim with a cryptographic ally sound proof upon request.

Unlike this project, uPort does not aim to determine consensus among different identities. All it cares is that "someone claimed X". However, this project can build upon uPort to shorten development cycle and prevent reinventing the wheel.

## Related work 3
[AUGUR](http://blog.augur.net/guide-to-augurs-rep/) is a forecasting system built on top of Ethereum. The basic principle is that a token is issued for each possible outcome, and the price of these tokens correlates to the "crowd-guessed possibility" of these outcomes. With a "winner takes all" approach, those who guessed correctly will get the tokens of those whose guess was wrong. REP is a specialised token used for reporting the outcomes of these guesses. Owners of REP are expected to report the outcomes of these events; those who do not report accurately or do not report at all will have their REP tokens removed and redistributed to others.

In contrast, our solution do not require a per-claim vote or consensus. Instead, the consensus is laid upon the claimer's trust in the network. This is done out of practical concerns: (1) it would be hard for others to check if one really broke his legs and (2) voting on each and every issue would be a huge burden to the users.

## The deliverable
The deliverable shall be a library that exhibits the following functions:

* Establish an identity
* Trust or "friend" another identity
* Make an assertion
* Determine whether a claim by an identity can be trusted

Furthermore, an example "kitchen sink" application showcasing each and every library function will be available.

At later stages of the project (e.g. in the second semester), interesting application of the said library, such as peer-to-peer insurance, may be developed.

## Schedule
| Milestone | Target | Date |
| --------- | ------ | ---- |
| 1         | Decide on the algorithm for determining trust on a claim | 2017-10-13 |
| 2         | Design the library's interface; interacts with a dummy registry | 2017-10-27 |
| 3         | Design the data structure & optimized algorithm for the central registry | 2017-11-10 |
| 4         | Update the library to use the new registry | 2017-11-17 |
| 5         | Develop a simple application to showcase the library's capabilities | 2017-11-30 |

## Targets for the Next Semester
1. Exemplar applications built on top of the library (deliverable in semester 1) such as P2P insurance